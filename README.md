# Parallelize Particle Swarm Optimization Algorithm
Please see in other branches to see instructions regard to how to compile the code, parallel methods and result.  
 
### What is PSO
PSO algorithm stands for particle swarm optimization which originated as a simulation of the choreography of birds or a bird block. PSO algorithm is used to solve the optimization problems. In PSO, each bird is seen as a particle. And each particle has its own fitness value which is evaluated by the fitness function to be optimized; and velocity value which direct the movement of the particle. Each particle moves through the space by following the velocity of the one particle which has the best fitness value.  
  
First PSO initializes a group of particles with randomly generated position, evaluate their fitness and record the initial best fitness as its own best fitness value(pBest). Then it will search for the particle with the best fitness and record the fitness as global best(gBest). In each iteration, each particle is updated by following these two best values. When a particle takes part of the population as its topological neighbors, the best value is called lBest. After a particle find gBest and pBest, it will update its velocity and position by following equations a) for velocity and b) for position.   
```
a)	v[] = v[] + c1 * rand() * (pbest[] - present[]) + c2 * rand() * (gbest[] - present[]) 
b)	present[] = persent[] + v[] 
```
In these equation: rand() is a random number between 0 and 1. C1, and c2 are learning factors which usually is c1 = c2 = 2.  
  
- Here is a pseudo code for the algorithm from http://www.swarmintelligence.org/tutorials.php
```
For each particle 
    Initialize particle
END

Do
    For each particle 
        Calculate fitness value
        If the fitness value is better than the best fitness value (pBest) in history
            set current value as the new pBest
    End

    Choose the particle with the best fitness value of all the particles as the gBest
    For each particle 
        Calculate particle velocity according equation (a)
        Update particle position according equation (b)
    End 
While maximum iterations or minimum error criteria is not attained
```